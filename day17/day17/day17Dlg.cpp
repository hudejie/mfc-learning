
// day17Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "day17.h"
#include "day17Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cday17Dlg 对话框




Cday17Dlg::Cday17Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cday17Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cday17Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_RED, m_chk_red);
	DDX_Control(pDX, IDC_CHECK_GREEN, m_chk_green);
	DDX_Control(pDX, IDC_CHECK_BLUE, m_chk_blue);
	DDX_Control(pDX, IDC_RADIO_SQURAE, m_rd_squ);
	DDX_Control(pDX, IDC_RADIO_CIRC, m_rd_circ);
}

BEGIN_MESSAGE_MAP(Cday17Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CHECK_BLUE, &Cday17Dlg::OnBnClickedCheckBlue)
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_CHECK_GREEN, &Cday17Dlg::OnBnClickedCheckGreen)
	ON_BN_CLICKED(IDC_CHECK_RED, &Cday17Dlg::OnBnClickedCheckRed)
	ON_BN_CLICKED(IDC_RADIO_SQURAE, &Cday17Dlg::OnBnClickedRadioSqurae)
	ON_BN_CLICKED(IDC_RADIO_CIRC, &Cday17Dlg::OnBnClickedRadioCirc)
END_MESSAGE_MAP()


// Cday17Dlg 消息处理程序

BOOL Cday17Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	CheckRadioButton(IDC_RADIO_SQURAE,IDC_RADIO_CIRC,IDC_RADIO_CIRC);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void Cday17Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cday17Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cday17Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Cday17Dlg::OnBnClickedCheckBlue()
{
	// TODO: 在此添加控件通知处理程序代码
	Invalidate();
}


BOOL Cday17Dlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	BOOL bRet = CDialogEx::OnEraseBkgnd(pDC);
	int nRed,nGreen,nBlue;
	nRed = m_chk_red.GetCheck() ? 255 : 0;
	nGreen = m_chk_green.GetCheck() ? 255 : 0;
	nBlue = m_chk_blue.GetCheck() ? 255 : 0;

	COLORREF crFore = RGB(nRed,nGreen,nBlue);
	CBrush brush;
	brush.CreateSolidBrush(crFore);
	CBrush *pOldBrush = pDC->SelectObject(&brush);

	RECT rc = {100, 40, 400, 340};
	if (m_rd_squ.GetCheck())
	{
		pDC->Rectangle(&rc);
	}
	else{
		pDC->Ellipse(&rc);
	}
	pDC->SelectObject(pOldBrush);
	return bRet;
}


void Cday17Dlg::OnBnClickedCheckGreen()
{
	// TODO: 在此添加控件通知处理程序代码
	OnBnClickedCheckBlue();
}


void Cday17Dlg::OnBnClickedCheckRed()
{
	// TODO: 在此添加控件通知处理程序代码
	OnBnClickedCheckBlue();
}


void Cday17Dlg::OnBnClickedRadioSqurae()
{
	// TODO: 在此添加控件通知处理程序代码
	OnBnClickedCheckBlue();
}


void Cday17Dlg::OnBnClickedRadioCirc()
{
	// TODO: 在此添加控件通知处理程序代码
	OnBnClickedCheckBlue();
}
