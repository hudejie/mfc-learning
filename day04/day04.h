// day04.h : main header file for the DAY04 application
//

#if !defined(AFX_DAY04_H__FC3ACB79_4955_4167_AA1C_991D45AB0B7F__INCLUDED_)
#define AFX_DAY04_H__FC3ACB79_4955_4167_AA1C_991D45AB0B7F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDay04App:
// See day04.cpp for the implementation of this class
//

class CDay04App : public CWinApp
{
public:
	CDay04App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDay04App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDay04App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAY04_H__FC3ACB79_4955_4167_AA1C_991D45AB0B7F__INCLUDED_)
