//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by day13.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DAY13_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_16                   131
#define IDB_BITMAP_32                   132
#define IDC_LIST_VIEW                   1000
#define IDC_BUTTON_ADD                  1001
#define IDC_BUTTON_DEL                  1002
#define IDC_COMBO2                      1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
