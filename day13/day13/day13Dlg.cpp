
// day13Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "day13.h"
#include "day13Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cday13Dlg 对话框




Cday13Dlg::Cday13Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cday13Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cday13Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_VIEW, m_list);
}

BEGIN_MESSAGE_MAP(Cday13Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_ADD, &Cday13Dlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &Cday13Dlg::OnBnClickedButtonDel)
	ON_CBN_SELCHANGE(IDC_COMBO2, &Cday13Dlg::OnCbnSelchangeCombo2)
END_MESSAGE_MAP()


// Cday13Dlg 消息处理程序

BOOL Cday13Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	m_list.ModifyStyle(LVS_TYPEMASK, LVS_REPORT); //设置列表控件样式为详细信息
	m_list.InsertColumn(0,_T("第一列"),LVCFMT_LEFT,220);
	m_list.InsertColumn(1,_T("第二列"),LVCFMT_LEFT,220);
	m_list.InsertColumn(2,_T("第三列"),LVCFMT_LEFT,220);

	m_ilLarge.Create(IDB_BITMAP_32, 32, 1,RGB(255,0,255));
	m_ilSmall.Create(IDB_BITMAP_16, 16, 1,RGB(255,0,255));

	m_list.SetImageList(&m_ilLarge,LVSIL_NORMAL);
	m_list.SetImageList(&m_ilSmall,LVSIL_SMALL);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void Cday13Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cday13Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cday13Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Cday13Dlg::OnBnClickedButtonAdd()
{
	// TODO: 在此添加控件通知处理程序代码
	int nCount = m_list.GetItemCount();
	CString strText;
	strText.Format(_T("第%d行，第一列"), nCount + 1);
	m_list.InsertItem(nCount,strText,0);
	strText.Format(_T("第%d行，第二列"), nCount + 1);
	m_list.SetItemText(nCount,1,strText);
	strText.Format(_T("第%d行，第三列"), nCount + 1);
	m_list.SetItemText(nCount,2,strText);

}


void Cday13Dlg::OnBnClickedButtonDel()
{
	// TODO: 在此添加控件通知处理程序代码
	int nCount = m_list.GetItemCount();
	for (int i = nCount; i>=0 ; --i)
	{
		if (m_list.GetItemState(i,LVIS_SELECTED) == LVIS_SELECTED)
		{
			m_list.DeleteItem(i);
		}
	}
}



void Cday13Dlg::OnCbnSelchangeCombo2()
{
	// TODO: 在此添加控件通知处理程序代码
	DWORD dwStyle;
	CComboBox *pView;
	pView = (CComboBox *)GetDlgItem(IDC_COMBO2);
	switch(pView->GetCurSel())
	{
	case 0:
		dwStyle = LVS_ICON;
		break;
	case 1:
		dwStyle = LVS_SMALLICON;
		break;
	case 2:
		dwStyle = LVS_LIST;
		break;
	case 3:
		dwStyle = LVS_REPORT;
		break;
	default:
		return;
	}
	m_list.ModifyStyle(LVS_TYPEMASK,dwStyle);
}
