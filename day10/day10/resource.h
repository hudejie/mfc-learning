//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by day10.rc
//
#define IDD_DAY10_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDR_MENU_SYSTEM                 129
#define IDR_MENU_QUICK                  131
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_32777                        32777
#define ID_32778                        32778
#define ID_32779                        32779
#define ID_32780                        32780
#define ID_32781                        32781
#define ID_32782                        32782
#define ID_32783                        32783
#define ID_COPY                         32784

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32785
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
