// day01.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"
#include "stdio.h"


BOOL CALLBACK DlGFUNC(HWND hwnd_dlg,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_COMMAND:
		switch(wParam)
		{
		case IDCANCEL:
			EndDialog(hwnd_dlg,IDCANCEL);
			break;
		case IDOK:
			{
				int nHeight= GetDlgItemInt(hwnd_dlg,IDC_EDIT_SG,NULL,TRUE);
				double dHeight = nHeight / 100.00;
				int nWeight = GetDlgItemInt(hwnd_dlg,IDC_EDIT_TZ,NULL,TRUE);
				double bmi = nWeight/(dHeight*dHeight);
				char buf[28];
				sprintf(buf, "%.3f", bmi);
				SetDlgItemText(hwnd_dlg,IDC_EDIT_BMI,buf);
			}
			break;
		}
		break;
	}
	return FALSE;
}

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	DialogBox(hInstance,(LPCTSTR)IDD_DIALOG_BMI,NULL,DlGFUNC);

	return 0;
}



