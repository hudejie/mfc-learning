//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by day01.rc
//
#define IDD_DIALOG_BMI                  101
#define IDC_STATIC_SG                   1000
#define IDC_EDIT_SG                     1001
#define IDC_STATIC_TZ                   1002
#define IDC_EDIT_TZ                     1003
#define IDC_EDIT3                       1005
#define IDC_EDIT_BMI                    1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
