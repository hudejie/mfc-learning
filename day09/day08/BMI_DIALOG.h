#pragma once
#include "afxwin.h"
#include "stdio.h"


// BMI_DIALOG 对话框

class BMI_DIALOG : public CDialogEx
{
	DECLARE_DYNAMIC(BMI_DIALOG)

public:
	BMI_DIALOG(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~BMI_DIALOG();

// 对话框数据
	enum { IDD = IDD_DAY08_BMI_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edit_sg;
	CEdit m_edit_tz;
	CEdit m_edit_bmi;
	int m_Height;
	int m_Weight;
	double m_BMI;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonFunc4();
	afx_msg void OnBnClickedButtonFunc5();
	afx_msg void OnBnClickedButtonFunc7();
};
