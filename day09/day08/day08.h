
// day08.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号


// Cday08App:
// 有关此类的实现，请参阅 day08.cpp
//

class Cday08App : public CWinApp
{
public:
	Cday08App();

// 重写
public:
	virtual BOOL InitInstance();

// 实现

	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedButtonFunc6();
};

extern Cday08App theApp;