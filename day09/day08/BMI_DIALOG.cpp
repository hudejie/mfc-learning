// BMI_DIALOG.cpp : 实现文件
//

#include "stdafx.h"
#include "day08.h"
#include "BMI_DIALOG.h"
#include "afxdialogex.h"


// BMI_DIALOG 对话框

IMPLEMENT_DYNAMIC(BMI_DIALOG, CDialogEx)

BMI_DIALOG::BMI_DIALOG(CWnd* pParent /*=NULL*/)
	: CDialogEx(BMI_DIALOG::IDD, pParent)
	, m_Height(0)
	, m_Weight(0)
	, m_BMI(0)
{

}

BMI_DIALOG::~BMI_DIALOG()
{
}

void BMI_DIALOG::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_SG, m_edit_sg);
	DDX_Control(pDX, IDC_EDIT_TZ, m_edit_tz);
	DDX_Control(pDX, IDC_EDIT_BMI, m_edit_bmi);
	DDX_Text(pDX, IDC_EDIT_SG, m_Height);
	DDX_Text(pDX, IDC_EDIT_TZ, m_Weight);
	DDX_Text(pDX, IDC_EDIT_BMI, m_BMI);
}


BEGIN_MESSAGE_MAP(BMI_DIALOG, CDialogEx)
	ON_BN_CLICKED(IDOK, &BMI_DIALOG::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_FUNC4, &BMI_DIALOG::OnBnClickedButtonFunc4)
	ON_BN_CLICKED(IDC_BUTTON_FUNC5, &BMI_DIALOG::OnBnClickedButtonFunc5)
	ON_BN_CLICKED(IDC_BUTTON_FUNC7, &BMI_DIALOG::OnBnClickedButtonFunc7)
END_MESSAGE_MAP()


// BMI_DIALOG 消息处理程序


void BMI_DIALOG::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码

	/* 第一种
	int nHeight= GetDlgItemInt(IDC_EDIT_SG,NULL,TRUE);
	double dHeight = nHeight / 100.00;
	int nWeight = GetDlgItemInt(IDC_EDIT_TZ,NULL,TRUE);
	double bmi = nWeight/(dHeight*dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);
	SetDlgItemText(IDC_EDIT_BMI,buf);
	*/

	/* 第二种
	TCHAR tcHeight[10], tcWeight[10] , tcBMI[10];
	GetDlgItem(IDC_EDIT_SG)->GetWindowText(tcHeight,10);
	GetDlgItem(IDC_EDIT_TZ)->GetWindowText(tcWeight,10);

	int nHeight = _ttoi(tcHeight);
	double dHeight = nHeight / 100.00;
	int nWeight = _ttoi(tcWeight);
	
	double bmi = nWeight/(dHeight * dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);
	GetDlgItem(IDC_EDIT_BMI)->SetWindowText(buf);
	*/

	TCHAR tcHeight[10], tcWeight[10] , tcBMI[10];
	GetDlgItemText(IDC_EDIT_SG,tcHeight,10);
	GetDlgItemText(IDC_EDIT_TZ,tcWeight,10);

	int nHeight = _ttoi(tcHeight);
	double dHeight = nHeight / 100.00;
	int nWeight = _ttoi(tcWeight);
	
	double bmi = nWeight/(dHeight * dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);
	SetDlgItemText(IDC_EDIT_BMI,buf);
}


void BMI_DIALOG::OnBnClickedButtonFunc4()
{
	// TODO: 在此添加控件通知处理程序代码
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);   //把控件的值关联到变量
	double dHeight = m_Height / 100.00;
	m_BMI = (m_Weight / (dHeight * dHeight ));
	UpdateData(FALSE);  //把变量的值关联到控件
}


void BMI_DIALOG::OnBnClickedButtonFunc5()
{
	// TODO: 在此添加控件通知处理程序代码
	TCHAR tcHeight[10], tcWeight[10] , tcBMI[10];
	m_edit_sg.GetWindowText(tcHeight,10);
	m_edit_tz.GetWindowText(tcWeight,10);

	int nHeight = _ttoi(tcHeight);
	double dHeight = nHeight / 100.00;
	int nWeight = _ttoi(tcWeight);

	double bmi = nWeight/(dHeight * dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);
	m_edit_bmi.SetWindowText(buf);
}


void BMI_DIALOG::OnBnClickedButtonFunc7()
{
	// TODO: 在此添加控件通知处理程序代码


}
