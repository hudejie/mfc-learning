
// day08Dlg.h : 头文件
//
#include "stdio.h"

#pragma once


// Cday08Dlg 对话框
class Cday08Dlg : public CDialogEx
{
// 构造
public:
	Cday08Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_DAY08_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	int m_Height;
	int m_Weight;
	double m_BMI;
	CEdit m_edit_sg;
	CEdit m_edit_tz;
	CEdit m_edit_bmi;
	afx_msg void OnBnClickedButtonFunc4();
	afx_msg void OnBnClickedButtonFunc5();
	afx_msg void OnBnClickedButtonFunc6();
	afx_msg void OnBnClickedButtonFunc7();
};
