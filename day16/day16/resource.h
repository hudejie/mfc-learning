//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by day16.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DAY16_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDR_MENU_MAIN                   129
#define IDC_EDIT_TXT                    1000
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
