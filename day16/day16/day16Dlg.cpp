
// day16Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "day16.h"
#include "day16Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cday16Dlg 对话框




Cday16Dlg::Cday16Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cday16Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cday16Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Cday16Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(ID_32772, &Cday16Dlg::OnFileOpen)
	ON_COMMAND(ID_32773, &Cday16Dlg::OnSaveFile)
END_MESSAGE_MAP()


// Cday16Dlg 消息处理程序

BOOL Cday16Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void Cday16Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cday16Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cday16Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Cday16Dlg::OnFileOpen()
{
	// TODO: 在此添加命令处理程序代码
	CFileDialog dlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST,_T("文本文件(*.txt)|*.txt|所有文件(*.*)|*.*||"),this);
	dlg.m_ofn.lpstrInitialDir = _T("C:\\");
	if (dlg.DoModal()  == IDOK)
	{
		CStdioFile inFile;
		inFile.Open(dlg.GetPathName(),CFile::modeRead);
		CString text;
		CString temp;
		while(inFile.ReadString(text))
		{    
			GetDlgItemText(IDC_EDIT_TXT,temp);
			if (temp.GetLength() > 0)
			{
				SetDlgItemText(IDC_EDIT_TXT,temp +"\r\n"+ text);
			}
			else
			{
				SetDlgItemText(IDC_EDIT_TXT,text);
			}
			
		}

		inFile.Close();
	}
}


void Cday16Dlg::OnSaveFile()
{
	// TODO: 在此添加命令处理程序代码
	CFileDialog dlg(FALSE,NULL,NULL,OFN_HIDEREADONLY |OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST,_T("文本文件(*.txt)|*.txt||"),this);
	if (dlg.DoModal()  == IDOK)
	{
		CFile file;  //建立一个CFile对象
		//打开文件,如果不存在该文件就创建文件
		if(!file.Open(dlg.GetPathName(),CFile::modeCreate|CFile::modeWrite))
		{
			AfxMessageBox( "can   not   Create   file! ");
			return;
		} 

		CString output;
		GetDlgItemText(IDC_EDIT_TXT,output);
		file.Write(output,strlen(output));
		file.Flush();    //将在缓冲区中的字符写入文件中
		file.Close();   //关闭文件
		AfxMessageBox( "保存成功！");

	}
}
