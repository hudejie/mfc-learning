//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by day08.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DAY08_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDC_STATIC_BMI                  1000
#define IDC_EDIT_SG                     1001
#define IDC_EDIT2                       1002
#define IDC_EDIT_TZ                     1002
#define IDC_STATIC_SG                   1003
#define IDC_EDIT_BMI                    1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
