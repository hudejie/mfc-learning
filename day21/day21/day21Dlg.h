
// day21Dlg.h : 头文件
//

#pragma once

#define WM_MY_FRIST_MSG (WM_USER + 100)

#define WM_MY_REGISTER_MSG_SRR _T("WM_MY_REGISTER_MSG_SRR")

// Cday21Dlg 对话框
class Cday21Dlg : public CDialogEx
{
// 构造
public:
	Cday21Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_DAY21_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnDefined();
	afx_msg LRESULT OnMyRegisterMsgTrigger(WPARAM wParam,  LPARAM lParam); 
	afx_msg LRESULT OnMyFirstMsgTrigger(WPARAM wParam,  LPARAM lParam); 
	afx_msg void OnBnClickedBtnReg();
};
