﻿# Windows编程基础 （CSDN博客教程源码）

[CSDN博客主页](http://blog.csdn.net/qq_25549309)

------

##### 系统环境

  - Win10 64bit i5
  - vc++ 6.0  / VS2010

------

#### 目录结构

  -  day01  （第1节 第一个win32软件 and 第2节 win32程序资源管理 ）
  --  [CSDN博客链接1](http://blog.csdn.net/qq_25549309/article/details/78267857)
  --  [CSDN博客链接2](http://blog.csdn.net/qq_25549309/article/details/78277801)
  - day02  （第3节 初探MFC）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78290196)
  - day03  （第4节 MFC消息映射机制）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78300083)
  - day04  （第5节 MFC对话框程序）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78307141)
  - day07  （第7节 来吧，VS 2010）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78323260)
  - day08  （第8节 MFC对话框控件访问的七种方法（上））
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78335269)
  - day09  （第9节 MFC对话框控件访问的七种方法（下））
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78345612)
  - day10  （第10节 MFC菜单）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78359647)
  - day13  （第11节 MFC之列表控件CListCtrl）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78380843)
  - day14  （第12节 MFC之树控件CTreeCtrl）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78396938)
  - day15  （第13节 MFC之图片控件）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78408722)
  - day16  （第14节 MFC之文件对话框CFileDialog）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78418291)
  - day17  （第15节 MFC之复选框及单选按钮）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78429760)
  - day18  （第16节 MFC之SendMessage和PostMessage）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78440576)
  - day19  （第17节 MFC之状态栏（CStatusBar））
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78451264)
  - day20  （第18节 MFC之窗体操作）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78461206)
  - day21  （第19节 MFC之自定义消息）
  --  [CSDN博客地址](http://blog.csdn.net/qq_25549309/article/details/78473074)
------


### 座右铭

我的梦想就是努力实现小时候吹过的牛。。。。

> 新人一枚，个人学习展示，不保证正确性，欢迎指正（Emai ：**AmuUncle@163.com**）!

### [>>>我的私人博客<<<](http://amuuncle.site/)

