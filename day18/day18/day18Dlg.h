
// day18Dlg.h : 头文件
//

#pragma once


// Cday18Dlg 对话框
class Cday18Dlg : public CDialogEx
{
// 构造
public:
	Cday18Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_DAY18_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	HCURSOR m_hCursor;  //定义一个鼠标的指针
	RECT m_rtCtrl;      //图标的大小
	BOOL m_bCapturing;   //鼠标捕获
	HWND m_hWndDest;   //目标窗口的窗口句柄

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedBtnSettext();
	afx_msg void OnBnClickedBtnSettext2();
};
