
// day20Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "day20.h"
#include "day20Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cday20Dlg 对话框




Cday20Dlg::Cday20Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cday20Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cday20Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Cday20Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &Cday20Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &Cday20Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &Cday20Dlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &Cday20Dlg::OnBnClickedButton4)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON5, &Cday20Dlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &Cday20Dlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &Cday20Dlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_CHECK1, &Cday20Dlg::OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_BUTTON8, &Cday20Dlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &Cday20Dlg::OnBnClickedButton9)
END_MESSAGE_MAP()


// Cday20Dlg 消息处理程序

BOOL Cday20Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	HICON hIcon = AfxGetApp()->LoadIcon(IDI_ICON_LOL);
	SetIcon(hIcon, TRUE);			// 设置大图标
	SetIcon(hIcon, FALSE);		// 设置小图标

	SetWindowText(_T("LOL"));


	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void Cday20Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cday20Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cday20Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Cday20Dlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	SendMessage(WM_SYSCOMMAND,SC_MAXIMIZE,0);
}


void Cday20Dlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	SendMessage(WM_SYSCOMMAND,SC_MINIMIZE,0);
}


void Cday20Dlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	SendMessage(WM_SYSCOMMAND,SC_RESTORE,0);
}


void Cday20Dlg::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	SendMessage(WM_CLOSE,0,0);
}


void Cday20Dlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if ( IDYES == MessageBox(_T("关闭窗口？"),NULL,MB_YESNO))
	{
		CDialogEx::OnClose();
	}
	
}


void Cday20Dlg::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
	MoveWindow(100,200,600,450);
}


void Cday20Dlg::OnBnClickedButton6()
{
	// TODO: 在此添加控件通知处理程序代码
	SetWindowPos(NULL,500,100,700,500,SWP_NOZORDER);
}


void Cday20Dlg::OnBnClickedButton7()
{
	// TODO: 在此添加控件通知处理程序代码
	CenterWindow();

}


void Cday20Dlg::OnBnClickedCheck1()
{
	// TODO: 在此添加控件通知处理程序代码
	BOOL bTop = static_cast<CButton*>(GetDlgItem(IDC_CHECK1))->GetCheck();
	if (bTop)
	{
		::SetWindowPos(m_hWnd,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
	}
	else
	{
		::SetWindowPos(m_hWnd,HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
	}
}


void Cday20Dlg::OnBnClickedButton8()
{
	// TODO: 在此添加控件通知处理程序代码
	TCHAR tcText[20];
	GetDlgItemText(IDC_EDIT_TEXT,tcText,20);
	SetWindowText(tcText);
}


void Cday20Dlg::OnBnClickedButton9()
{
	// TODO: 在此添加控件通知处理程序代码
	CString str;
	GetWindowText(str);
	AfxMessageBox(str);
}
